package org.tensorflow.lite.examples.detection.customview

import android.app.Activity
import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import org.tensorflow.lite.examples.detection.R
import org.tensorflow.lite.examples.detection.models.Component

class ComponentOverlay(
    private val activity: Activity,
    private val view: View
) : Overlay {

    private val textView = view.findViewById<TextView>(R.id.txt_title)
    private val txtMessage = view.findViewById<TextView>(R.id.txt_message)
    private val rvScannedItems = view.findViewById<RecyclerView>(R.id.rv_scanned_items)


    override fun showOverlay(list: List<Component>, message: String) {
        super.showOverlay(list, message)
        rvScannedItems.adapter = ComponentListAdapter(list)
        txtMessage.visibility = View.VISIBLE
        txtMessage.text = message
    }

    // Make sure this function is called from UI thread
    override fun hide() {
        activity.runOnUiThread {
            if (view.visibility == View.VISIBLE) {
                view.visibility = View.GONE
            }
        }
    }
}