/* Copyright 2019 The TensorFlow Authors. All Rights Reserved.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
==============================================================================*/

package org.tensorflow.lite.examples.detection.tracking;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Join;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.text.TextPaint;
import android.util.Log;
import android.util.Pair;
import android.util.TypedValue;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;

import org.tensorflow.lite.examples.detection.R;
import org.tensorflow.lite.examples.detection.customview.Overlay;
import org.tensorflow.lite.examples.detection.customview.TutorialView;
import org.tensorflow.lite.examples.detection.env.BorderedText;
import org.tensorflow.lite.examples.detection.env.ImageUtils;
import org.tensorflow.lite.examples.detection.env.Logger;
import org.tensorflow.lite.examples.detection.utility.Utility;
import org.tensorflow.lite.task.vision.detector.Detection;

/**
 * A tracker that handles non-max suppression and matches existing objects to new detections.
 */
public class MultiBoxTracker {
    private static final float TEXT_SIZE_DIP = 14;
    private static final float MIN_SIZE = 16.0f;
    private static final int[] COLORS = {
            Color.BLUE,
            Color.RED,
            Color.GREEN,
            Color.YELLOW,
            Color.CYAN,
            Color.MAGENTA,
            Color.WHITE,
            Color.parseColor("#55FF55"),
            Color.parseColor("#FFA500"),
            Color.parseColor("#FF8888"),
            Color.parseColor("#AAAAFF"),
            Color.parseColor("#FFFFAA"),
            Color.parseColor("#55AAAA"),
            Color.parseColor("#AA33AA"),
            Color.parseColor("#0D0068")
    };
    final List<Pair<Float, RectF>> screenRects = new LinkedList<Pair<Float, RectF>>();
    private final Logger logger = new Logger();
    private final Queue<Integer> availableColors = new LinkedList<Integer>();
    private final List<TrackedRecognition> trackedObjects = new LinkedList<TrackedRecognition>();
    private final List<TrackedRecognition> expectedObjects = new LinkedList<TrackedRecognition>();
    private final Paint boxPaint = new Paint();
    private final float textSizePx;
    private final BorderedText borderedText;
    private Matrix frameToCanvasMatrix;
    private int frameWidth;
    private int frameHeight;
    private int sensorOrientation;

    private Bitmap bitmapGuide;

    private RectF trackedRectFForTutorialView;

    private TextPaint textPaint = new TextPaint();

    public MultiBoxTracker(final Context context) {
        for (final int color : COLORS) {
            availableColors.add(color);
        }

        boxPaint.setColor(Color.RED);
        boxPaint.setStyle(Style.STROKE);
        boxPaint.setStrokeWidth(8.0f);
        boxPaint.setStrokeCap(Cap.ROUND);
        boxPaint.setStrokeJoin(Join.ROUND);
        boxPaint.setAntiAlias(true);
        boxPaint.setStrokeMiter(10);

        textSizePx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, context.getResources().getDisplayMetrics());
        borderedText = new BorderedText(textSizePx);
        initTextPaint();

        bitmapGuide = BitmapFactory.decodeResource(context.getResources(), R.drawable.ic_launcher);
    }


    private void initTextPaint() {
        textPaint.setColor(Color.WHITE);
        textPaint.setTextAlign(Paint.Align.CENTER);
        textPaint.setTextSize(textSizePx);
        textPaint.setAntiAlias(true);
        textPaint.setStrokeWidth(0.5f);
    }

    public synchronized void setFrameConfiguration(
            final int width, final int height, final int sensorOrientation) {
        frameWidth = width;
        frameHeight = height;
        this.sensorOrientation = sensorOrientation;
    }

    public synchronized void drawDebug(final Canvas canvas) {
        final Paint textPaint = new Paint();
        textPaint.setColor(Color.WHITE);
        textPaint.setTextSize(60.0f);

        final Paint boxPaint = new Paint();
        boxPaint.setColor(Color.RED);
        boxPaint.setAlpha(200);
        boxPaint.setStyle(Style.STROKE);

        for (final Pair<Float, RectF> detection : screenRects) {
            final RectF rect = detection.second;
            canvas.drawRect(rect, boxPaint);
            canvas.drawText("" + detection.first, rect.left, rect.top, textPaint);

            borderedText.drawText(canvas, rect.centerX(), rect.centerY(), "" + detection.first);
        }
    }

    public synchronized void trackResults(final List<Detection> results, final long timestamp) {
        logger.i("Processing %d results from %d", results.size(), timestamp);
        processResults(results);
    }

    private Matrix getFrameToCanvasMatrix() {
        return frameToCanvasMatrix;
    }

    public synchronized void draw(final Canvas canvas) {
        final boolean rotated = sensorOrientation % 180 == 90;
        final float multiplier =
                Math.min(
                        canvas.getHeight() / (float) (rotated ? frameWidth : frameHeight),
                        canvas.getWidth() / (float) (rotated ? frameHeight : frameWidth));
        frameToCanvasMatrix =
                ImageUtils.getTransformationMatrix(
                        frameWidth,
                        frameHeight,
                        (int) (multiplier * (rotated ? frameHeight : frameWidth)),
                        (int) (multiplier * (rotated ? frameWidth : frameHeight)),
                        sensorOrientation,
                        false);
        boolean isValidationRequired = TrackingManager.INSTANCE.isValidationRequiredForCurrentStep();
        Log.i(getClass().getSimpleName(), "Is Validation Required: " + isValidationRequired);
        // First Clear all expected objects
        expectedObjects.clear();
        for (final TrackedRecognition recognition : trackedObjects) {
            final RectF trackedPos = new RectF(recognition.location);
            getFrameToCanvasMatrix().mapRect(trackedPos);
            boxPaint.setColor(recognition.color);
            final String labelString = recognition.title;
            float cornerSize = Math.min(trackedPos.width(), trackedPos.height()) / 28.0f;

            if (isValidationRequired) {
                boolean isObjectInSuccessList = TrackingManager.INSTANCE.isLabelInSuccessList(labelString);
                if (isObjectInSuccessList) {
                    canvas.drawRoundRect(trackedPos, cornerSize, cornerSize, boxPaint);
                    drawText(canvas, trackedPos, labelString);
                    expectedObjects.add(recognition);
                }
                boolean isObjectInFailureList = TrackingManager.INSTANCE.isLabelInFailureList(labelString);
                if (isObjectInFailureList) {
                    boxPaint.setColor(Color.RED);
                    canvas.drawRoundRect(trackedPos, cornerSize, cornerSize, boxPaint);
                    drawText(canvas, trackedPos, labelString);
                    expectedObjects.add(recognition);
                }

            } else if (TrackingManager.INSTANCE.getCurrentStepOverlayType().equals(Overlay.OVERLAY_ON_SCREEN_GUIDE)) {
                boolean isObjectAvailable = TrackingManager.INSTANCE.isObjectAvailableInRequiredItems(labelString);
                if (isObjectAvailable) {
                    //canvas.drawRoundRect(trackedPos, cornerSize, cornerSize, boxPaint);
                    //drawText(canvas, trackedPos, labelString);
                    expectedObjects.add(recognition);
                    trackedRectFForTutorialView = trackedPos;
                }
            } else {
                canvas.drawRoundRect(trackedPos, cornerSize, cornerSize, boxPaint);
                drawText(canvas, trackedPos, labelString);
                expectedObjects.add(recognition);
            }
        }
        if (trackedObjects.isEmpty()) {
            trackedRectFForTutorialView = null;
        }
    }

    private void drawText(Canvas canvas, RectF rectF, String label) {
        float textHeight = textPaint.descent() - textPaint.ascent();
        float textOffset = textHeight / 2 - textPaint.descent();
        int margin = 10;
        RectF textRectF = new RectF(rectF.left, rectF.top - (textHeight + 20), rectF.right, rectF.top - 5);
        canvas.drawRoundRect(textRectF, 15f, 15f, new Paint());

        //Rect rect = new Rect((int) rectF.left, (int) (rectF.top + textHeight), (int) rectF.right, (int) rectF.right);
        // canvas.drawRect(rect, textPaint);
       /* int noOfChar = textPaint.breakText(
                label,
                true,
                rectF.width() - margin,
                null
        );*/

        canvas.drawText(Utility.getFormattedLabel(label), textRectF.centerX(), textRectF.centerY() + textOffset, textPaint);

    }

    public List<TrackedRecognition> getExpectedObjects() {
        return expectedObjects;
    }

    synchronized public void showTutorialView(TutorialView view, int canvasWidth) {
        view.resetTutorial();
        if (trackedRectFForTutorialView != null) {
            view.drawTutorial(trackedRectFForTutorialView, canvasWidth);
        }
    }

    private void processResults(final List<Detection> results) {
        final List<Pair<Float, Detection>> rectsToTrack = new LinkedList<Pair<Float, Detection>>();

        screenRects.clear();
        final Matrix rgbFrameToScreen = new Matrix(getFrameToCanvasMatrix());

        for (final Detection detection : results) {
            if (detection.getBoundingBox() == null) {
                continue;
            }
            final RectF detectionFrameRect = new RectF(detection.getBoundingBox());

            final RectF detectionScreenRect = new RectF();
            rgbFrameToScreen.mapRect(detectionScreenRect, detectionFrameRect);

            logger.v("Result! Frame: " + detection.getBoundingBox() + " mapped to screen:" + detectionScreenRect);

            screenRects.add(new Pair<Float, RectF>(detection.getCategories().get(0).getScore(), detectionScreenRect));

            if (detectionFrameRect.width() < MIN_SIZE || detectionFrameRect.height() < MIN_SIZE) {
                logger.w("Degenerate rectangle! " + detectionFrameRect);
                continue;
            }

            rectsToTrack.add(new Pair<Float, Detection>(detection.getCategories().get(0).getScore(), detection));
        }

        trackedObjects.clear();
        if (rectsToTrack.isEmpty()) {
            logger.v("Nothing to track, aborting.");
            return;
        }

        for (final Pair<Float, Detection> potential : rectsToTrack) {
            final TrackedRecognition trackedRecognition = new TrackedRecognition();
            trackedRecognition.detectionConfidence = potential.first;
            trackedRecognition.location = new RectF(potential.second.getBoundingBox());
            trackedRecognition.title = potential.second.getCategories().get(0).getLabel();
            //trackedRecognition.color = COLORS[trackedObjects.size()];
            trackedRecognition.color = COLORS[2];
            trackedObjects.add(trackedRecognition);

            if (trackedObjects.size() >= COLORS.length) {
                break;
            }
        }
    }

    public static class TrackedRecognition {
        RectF location;
        float detectionConfidence;
        int color;
        String title;
    }
}
