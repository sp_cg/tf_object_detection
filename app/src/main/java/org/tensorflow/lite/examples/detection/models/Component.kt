package org.tensorflow.lite.examples.detection.models

data class Component(
    val title: String,
    val isDetected: Boolean
) {
    companion object {
        @JvmStatic
        fun isAllComponentDetected(list: List<Component>) = !list.any { !it.isDetected }
    }
}


