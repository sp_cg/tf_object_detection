package org.tensorflow.lite.examples.detection.customview

import android.content.Context
import android.graphics.*
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import org.tensorflow.lite.examples.detection.tracking.TrackingManager
import org.tensorflow.lite.examples.detection.utility.Utility
import android.animation.ObjectAnimator

import android.graphics.PathMeasure
import android.graphics.DashPathEffect

import android.graphics.PathEffect


class AnimatedLineView(
    context: Context?,
    attrs: AttributeSet?
) : View(context, attrs) {

    private var startX = 0f
    private var startY = 0f

    private var stopX = 0f
    private var stopY = 0f

    private var length = 0f
    private val path = Path()

    private val pathPaint = Paint().apply {
        val dashPath = DashPathEffect(floatArrayOf(10f, 5f), 10.0.toFloat())
        color = Color.YELLOW
        style = Paint.Style.STROKE
        pathEffect = dashPath
        strokeWidth = 8.0f
        isAntiAlias = true
    }


    fun setCoordinates(trackedRectF: RectF, overlayRectF: RectF, bitmap: Bitmap) {
        path.moveTo(
            (overlayRectF.width() - bitmap.width + (bitmap.width / 2)),
            (overlayRectF.height() - bitmap.height / 2)
        )
        path.lineTo((trackedRectF.centerX()), trackedRectF.top)

        val measure = PathMeasure(path, false)
        length = measure.length

        val intervals = floatArrayOf(length, length)

        val animator = ObjectAnimator.ofFloat(this, PHASE, 1.0f, 0.0f)
        animator.duration = 2000
        animator.start()

    }

    //is called by animtor object
    fun setPhase(phase: Float) {
        pathPaint.pathEffect = createPathEffect(length, phase, 0.0f)
        invalidate() //will calll onDraw
    }

    private fun createPathEffect(pathLength: Float, phase: Float, offset: Float): PathEffect {
        return DashPathEffect(
            floatArrayOf(pathLength, pathLength), (phase * pathLength).coerceAtLeast(1.0f)
        )
    }


    override fun onDraw(canvas: Canvas?) {
        canvas?.drawPath(path, pathPaint)
    }

    companion object {
        const val PHASE = "phase"
    }
}