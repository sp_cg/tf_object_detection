package org.tensorflow.lite.examples.detection.customview

import android.content.Context
import android.graphics.*
import android.text.TextPaint
import android.util.AttributeSet
import android.view.View
import org.tensorflow.lite.examples.detection.tracking.TrackingManager
import org.tensorflow.lite.examples.detection.utility.Utility

class ImageWithLabel(
    context: Context?,
    attrs: AttributeSet?
) : View(context, attrs) {

    private lateinit var bitmap: Bitmap
    private val rectPaint = Paint().apply {
        color = Color.parseColor("#BE141C0B")
        alpha = 180
        isAntiAlias = true
    }

    private val textPaint = TextPaint().apply {
        color = Color.WHITE
        textAlign = Paint.Align.CENTER
        this.textSize = 40f
        isAntiAlias = true
    }

    private lateinit var rect: RectF

    fun setCanvasData(width: Int, bitmap: Bitmap) {
        this.bitmap = bitmap
        rect = RectF(10.0f, 10.0f, width.toFloat(), 150.0f)
    }

    override fun onDraw(canvas: Canvas?) {
        val textHeight = textPaint.descent() - textPaint.ascent()
        val textOffset = textHeight / 2 - textPaint.descent()
        canvas?.let {
            canvas.drawRoundRect(rect, 10f, 10f, rectPaint)
            bitmap.let { bitmap ->
                // Bitmap
                val left = (rect.width() - bitmap.width)
                val top = (rect.height() - bitmap.height) / 2
                canvas.drawBitmap(bitmap, left, top, null)
            }

            val margin = 20f
            var text = TrackingManager.getActionInStringFormat()
            val numOfChars = textPaint.breakText(
                text,
                true,
                rect.width() - margin,
                null
            )
            if (text.length > numOfChars) {
                text = text.substring(0, numOfChars) + ".."
            }
            canvas.drawText(
                text,
                rect.centerX(),
                rect.centerY() + textOffset,
                textPaint
            )
        }
    }

}