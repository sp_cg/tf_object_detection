package org.tensorflow.lite.examples.detection.customview

import android.app.Activity
import android.view.View
import org.tensorflow.lite.examples.detection.R

object ToastViewFactory {

    fun getToastView(activity: Activity, type: String): Overlay {

        return if (type == Overlay.OVERLAY_SHOW_COMPONENT_LIST) {
            val view = activity.findViewById<View>(R.id.view_component_toast)
            view.visibility = View.VISIBLE
            ComponentOverlay(activity, view)
        } else {
            val view = activity.findViewById<View>(R.id.view_custom_toast)
            view.visibility = View.VISIBLE
            SimpleOverlay(activity, view)
        }

    }

}