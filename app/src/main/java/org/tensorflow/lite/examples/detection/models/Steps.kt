package org.tensorflow.lite.examples.detection.models;

data class Step(
    val stepId: Int,
    val stepName: String,
    val stepStatus: Int,
    val noObjectDetectedErrorMessage: String?,
    val objectMissingErrorMessage: String?,
    val isValidationRequired: Boolean,
    val requiredItems: List<String>,
    val positiveItems: List<String>,
    val negativeItems: List<String>,
    val instructions: List<String>,
    val action: Action?
) {
    companion object {
        const val SCAN_PRODUCT_COMPONENTS = "SCAN_PRODUCT_COMPONENTS"
        const val SCAN_OBJECTS = "SCAN_OBJECTS"
    }
}