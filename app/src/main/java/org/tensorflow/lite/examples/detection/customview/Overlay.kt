package org.tensorflow.lite.examples.detection.customview

import org.tensorflow.lite.examples.detection.models.Component

interface Overlay {


    fun showOverlay(message: String) {}
    fun showOverlay(list: List<Component>, message: String) {}
    fun hide()

    companion object {
        const val OVERLAY_SHOW_COMPONENT_LIST = "OVERLAY_SHOW_COMPONENT_LIST"
        const val OVERLAY_SIMPLE_MESSAGE = "OVERLAY_SIMPLE_MESSAGE"
        const val OVERLAY_SIMPLE_MESSAGE_AUTO_DISMISS = "OVERLAY_SIMPLE_MESSAGE_AUTO_DISMISS"
        const val OVERLAY_ON_SCREEN_GUIDE = "OVERLAY_ON_SCREEN_GUIDE"
    }
}