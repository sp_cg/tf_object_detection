package org.tensorflow.lite.examples.detection.dialogs

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.bumptech.glide.Glide
import org.tensorflow.lite.examples.detection.R
import org.tensorflow.lite.examples.detection.databinding.DialogActionBinding
import org.tensorflow.lite.examples.detection.databinding.DialogComponentsBinding
import org.tensorflow.lite.examples.detection.tracking.TrackingManager

class ActionDialog : DialogFragment() {

    var binding: DialogActionBinding? = null;

    var listener: DialogListener? = null


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    fun setCallback(callback: DialogListener) {
        this.listener = callback
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogActionBinding.inflate(layoutInflater)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        //binding?.bubbleView?.text = TrackingManager.getComponentsInString()

        loadGif()
        updateAction()

        binding?.buttonDone?.setOnClickListener {
            dialog?.dismiss()
            listener?.onButtonClick(true)
        }
    }

    private fun updateAction() {
        binding?.txtInstructions?.text = TrackingManager.getActionInStringFormat()
    }


    private fun loadGif() {
        binding?.imgGuide?.let {
            Glide.with(this).asGif().load(
                R.drawable.guide
            ).into(it)
        }


    }
}

interface DialogListener {
    fun onButtonClick(isDoneBtn: Boolean)
}