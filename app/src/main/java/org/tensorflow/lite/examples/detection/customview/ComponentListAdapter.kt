package org.tensorflow.lite.examples.detection.customview

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatCheckBox
import androidx.recyclerview.widget.RecyclerView
import org.tensorflow.lite.examples.detection.R
import org.tensorflow.lite.examples.detection.models.Component

class ComponentListAdapter(
    private val items: List<Component>
) : RecyclerView.Adapter<ComponentListAdapter.ViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.item_component, parent, false)
        )
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.checkBox.text = items[position].title.replace("_", " ")
        holder.checkBox.isChecked = items[position].isDetected

    }

    override fun getItemCount() = items.size

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val checkBox: AppCompatCheckBox = view.findViewById(R.id.chk_component)
    }
}