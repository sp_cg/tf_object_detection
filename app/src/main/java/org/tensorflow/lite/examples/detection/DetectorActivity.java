/*
 * Copyright 2019 The TensorFlow Authors. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tensorflow.lite.examples.detection;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.RectF;
import android.graphics.Typeface;
import android.media.ImageReader.OnImageAvailableListener;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.util.Size;
import android.util.TypedValue;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import org.tensorflow.lite.examples.detection.customview.Overlay;
import org.tensorflow.lite.examples.detection.customview.OverlayView;
import org.tensorflow.lite.examples.detection.customview.OverlayView.DrawCallback;
import org.tensorflow.lite.examples.detection.customview.ToastViewFactory;
import org.tensorflow.lite.examples.detection.customview.TutorialView;
import org.tensorflow.lite.examples.detection.dialogs.ActionDialog;
import org.tensorflow.lite.examples.detection.dialogs.ComponentsDialog;
import org.tensorflow.lite.examples.detection.env.BorderedText;
import org.tensorflow.lite.examples.detection.env.ImageUtils;
import org.tensorflow.lite.examples.detection.env.Logger;
import org.tensorflow.lite.examples.detection.models.Component;
import org.tensorflow.lite.examples.detection.models.ProductInfo;
import org.tensorflow.lite.examples.detection.models.ProductManager;
import org.tensorflow.lite.examples.detection.models.Step;
import org.tensorflow.lite.examples.detection.tracking.MultiBoxTracker;
import org.tensorflow.lite.examples.detection.tracking.TrackerListener;
import org.tensorflow.lite.examples.detection.tracking.TrackingManager;
import org.tensorflow.lite.examples.detection.utility.SpeechManager;
import org.tensorflow.lite.support.image.TensorImage;
import org.tensorflow.lite.task.vision.detector.Detection;
import org.tensorflow.lite.task.vision.detector.ObjectDetector;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * An activity that uses a TensorFlowMultiBoxDetector and ObjectTracker to detect and then track
 * objects.
 */
public class DetectorActivity extends CameraActivity implements OnImageAvailableListener {
    private static final Logger LOGGER = new Logger();

    // Configuration values for the prepackaged SSD model.
    private static final int TF_OD_API_INPUT_SIZE = 320;
    private static final boolean TF_OD_API_IS_QUANTIZED = false;
    private static final String TF_OD_API_MODEL_FILE = "detect.tflite";
    private static final String TF_OD_API_LABELS_FILE = "labelmap.txt";
    private static final DetectorMode MODE = DetectorMode.TF_OD_API;
    // Minimum detection confidence to track a detection.
    private static final float MINIMUM_CONFIDENCE_TF_OD_API = 0.5f;
    private static final boolean MAINTAIN_ASPECT = false;
    private static final Size DESIRED_PREVIEW_SIZE = new Size(640, 480);
    private static final boolean SAVE_PREVIEW_BITMAP = false;
    private static final float TEXT_SIZE_DIP = 10;
    OverlayView trackingOverlay;
    TutorialView tutorialView;

    private Integer sensorOrientation;

    //private Detector detector;

    private ObjectDetector detector;
    private ObjectDetector.ObjectDetectorOptions options;

    public static final int MAX_FRAME_COUNT = 50;

    private int currentFrameCount = 0;
    private int thresholdFrameCount = 0;
    private long lastProcessingTimeMs;
    private Bitmap rgbFrameBitmap = null;
    private Bitmap croppedBitmap = null;
    private Bitmap cropCopyBitmap = null;

    private boolean computingDetection = false;

    private long timestamp = 0;

    private Matrix frameToCropTransform;
    private Matrix cropToFrameTransform;

    private MultiBoxTracker tracker;


    private BorderedText borderedText;

    private boolean isStarted;

    private boolean isActionInProgress = false;
    private boolean isInstructionShowing = false;

    public static ProductInfo productInfo = ProductManager.getProductInfo();

    private SpeechManager speechManager;

    private final TrackerListener trackerListener = new TrackerListener() {

        @Override
        public void hideOverlayIfShowing() {
            runOnUiThread(() -> {
                if (tutorialView != null) {
                    tutorialView.resetTutorial();
                }
            });
        }

        @Override
        public void showGeneralError(@NonNull Step currentStep) {
            // TODO Need to Show Any Common Error
        }

        @Override
        public void initiateInstruction(@Nullable List<String> instructions) {
            // When We need to show any instructions for the steps
            if (instructions != null && instructions.get(0) != null) {
                isInstructionShowing = true;
                //  simpleToastView.showToast(instructions.get(0));
            }
        }

        @Override
        public void showNoItemsDetectedError(@NonNull Step currentStep) {
            runOnUiThread(() -> {
                resetDetected();
                updateButtonLabel(getString(R.string.retry));
                String actionType = currentStep.getAction().getOverlayType();
                if (actionType != null) {
                    switch (actionType) {
                        case Overlay.OVERLAY_SIMPLE_MESSAGE_AUTO_DISMISS:
                            overlay = ToastViewFactory.INSTANCE.getToastView(DetectorActivity.this, Overlay.OVERLAY_SIMPLE_MESSAGE_AUTO_DISMISS);
                            new Handler().postDelayed(() -> {
                                overlay.hide();
                            }, 1000);

                            break;
                        default:
                            overlay = ToastViewFactory.INSTANCE.getToastView(DetectorActivity.this, Overlay.OVERLAY_SIMPLE_MESSAGE);
                            overlay.showOverlay("" + currentStep.getNoObjectDetectedErrorMessage());
                            break;
                    }

                }
            });

        }

        @Override
        public void showObjectDetectionResult(boolean isObjectMatched,
                                              @NonNull Step currentStep,
                                              @NonNull MultiBoxTracker.TrackedRecognition detectedObject) {
            runOnUiThread(() -> {
                String overlayType = currentStep.getAction().getOverlayType();
                if (overlayType != null) {
                    if (overlayType.equalsIgnoreCase(Overlay.OVERLAY_ON_SCREEN_GUIDE)) {
                        if (isActionInProgress) {
                            return;
                        }
                        isActionInProgress = true;
                        thresholdFrameCount = 0;
                        final String message = currentStep.getAction().getSuccess().getDescription();
                        speechManager.speak(message);
                        progressBar.setVisibility(View.GONE);
                        tracker.showTutorialView(tutorialView, trackingOverlay.getWidth());
                        new Handler().postDelayed(() -> {
                            resetDetected();
                            updateButtonLabel(getString(R.string.what_next));
                            updateProgressStatus();
                            hideOverlayIfShowing();
                            isActionInProgress = false;
                        }, 6000);

                    } else {
                        resetDetected();
                        String message = currentStep.getAction().getSuccess().getDescription();
                        if (!isObjectMatched) {
                            message = currentStep.getAction().getFailure().getDescription();
                        }
                        overlay = ToastViewFactory.INSTANCE.getToastView(DetectorActivity.this, Overlay.OVERLAY_SIMPLE_MESSAGE);
                        overlay.showOverlay(message);
                        speechManager.speak(message);
                        updateButtonLabel(getString(R.string.what_next));
                    }
                }
                checkForLastStepAndUpdate(isObjectMatched);
            });
        }

        @Override
        public void showProductComponentScanResult(@NonNull List<Component> results, @NonNull Step currentStep) {
            runOnUiThread(() -> {
                overlay = ToastViewFactory.INSTANCE.getToastView(DetectorActivity.this, Overlay.OVERLAY_SHOW_COMPONENT_LIST);
                resetDetected();
                if (!Component.isAllComponentDetected(results)) {
                    //Failure
                    overlay.showOverlay(results, currentStep.getObjectMissingErrorMessage());
                    speechManager.speak(currentStep.getObjectMissingErrorMessage());
                    updateButtonLabel(getString(R.string.retry));
                } else {
                    // Success
                    overlay.showOverlay(results, currentStep.getAction().getSuccess().getDescription());
                    speechManager.speak(currentStep.getAction().getSuccess().getDescription());
                    updateButtonLabel(getString(R.string.what_next));
                }
            });

        }
    };

    private void checkForLastStepAndUpdate(boolean isObjectMatched) {
        if (TrackingManager.INSTANCE.isLastStep()) {
            if (!isObjectMatched) {
                AlertDialog.Builder builder1 = new AlertDialog.Builder(DetectorActivity.this);
                builder1.setMessage("Contact Customer Care ?");
                builder1.setCancelable(true);

                builder1.setPositiveButton(
                        "Yes",
                        (DialogInterface.OnClickListener) (dialog, id) -> {
                            Intent intent = new Intent(Intent.ACTION_SEND);
                            String[] recipients = {"mailto@spectrumCare.com"};
                            intent.putExtra(Intent.EXTRA_EMAIL, recipients);
                            intent.putExtra(Intent.EXTRA_SUBJECT, "Need help for router setup");
                            intent.putExtra(Intent.EXTRA_TEXT, "Hello Team, Kindly help me with setting up my all new spectrum router");
                            intent.putExtra(Intent.EXTRA_CC, "mailcc@gmail.com");
                            intent.setType("text/html");
                            intent.setPackage("com.google.android.gm");
                            startActivity(Intent.createChooser(intent, "Send mail"));
                            updateButtonLabel(getString(R.string.done));
                        }
                );
                builder1.setNegativeButton(
                        "No",
                        (dialog, id) -> {
                            dialog.cancel();
                            updateButtonLabel(getString(R.string.retry));
                        });

                AlertDialog alert11 = builder1.create();
                alert11.show();
            } else {
                updateButtonLabel(getString(R.string.done));
            }
        }
    }


    private void updateButtonLabel(String newLabel) {
        runOnUiThread(() -> {
            btnStart.setText(newLabel);
        });
    }


    @Override
    public synchronized void onDestroy() {
        speechManager.onDestroy();
        ProductManager.resetProductInfo();
        isStarted = false;
        resetDetected();
        super.onDestroy();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        speechManager = new SpeechManager(DetectorActivity.this);
        TrackingManager.INSTANCE.setUp(trackerListener);
        btnStart.setOnClickListener(v -> {
            if (btnStart.getText().toString().equalsIgnoreCase(getString(R.string.stop_scanning))) {
                resetDetected();
                updateButtonLabel(getString(R.string.start_scan));
                updateProgressStatus();
            } else if (btnStart.getText().toString().equalsIgnoreCase(getString(R.string.what_next))
                    || (btnStart.getText().toString().equalsIgnoreCase(getString(R.string.ok)))
            ) {
                TrackingManager.INSTANCE.moveToNextStep();
                overlay.hide();
                thresholdFrameCount = 0;
                isStarted = true;
                updateButtonLabel(getString(R.string.stop_scanning));
                updateProgressStatus();
            } else if (btnStart.getText().toString().equalsIgnoreCase(getString(R.string.start_scan))) {
                thresholdFrameCount = 0;
                isStarted = true;
                updateButtonLabel(getString(R.string.stop_scanning));
                updateProgressStatus();
            } else if (btnStart.getText().toString().equalsIgnoreCase(getString(R.string.retry))) {
                // Need to handle when click
                thresholdFrameCount = 0;
                isStarted = true;
                overlay.hide();
                updateButtonLabel(getString(R.string.stop_scanning));
                updateProgressStatus();
            } else if (btnStart.getText().toString().equalsIgnoreCase(getString(R.string.done))) {
                ProductManager.resetProductInfo();
                isStarted = false;
                resetDetected();
                btnStart.setText(getString(R.string.get_started));
                overlay.hide();
            } else {
                isStarted = !isStarted;
                if (isStarted) {
                    thresholdFrameCount = 0;
                    TrackingManager.INSTANCE.moveToNextStep();
                    updateButtonLabel(getString(R.string.stop_scanning));
                }
                updateProgressStatus();
            }


        });

    }

    private void updateProgressStatus() {
        runOnUiThread(() -> {
            int visible = isStarted ? View.VISIBLE : View.GONE;
            progressBar.setVisibility(visible);

            /*int currentStep = TrackingManager.INSTANCE.getCurrentStepIndex();
            if (currentStep >= 1) {
                btnStart.setText(getString(R.string.what_next));
            } else {
                btnStart.setText(getString(R.string.get_started));
            }*/

        });
    }

    private void showComponentDialog() {
        ComponentsDialog dialog = new ComponentsDialog();
        dialog.show(getSupportFragmentManager(), "Component");
    }

    private void showActionDialog() {
        final ActionDialog actionDialog = new ActionDialog();

        actionDialog.setCallback(isDoneBtn -> {
            if (isDoneBtn) {
                ProductManager.resetProductInfo();
                isStarted = false;
                resetDetected();
            }
        });

        actionDialog.show(getSupportFragmentManager(), "ActionDialog");
    }


    @Override
    public void onPreviewSizeChosen(final Size size, final int rotation) {
        final float textSizePx = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, TEXT_SIZE_DIP, getResources().getDisplayMetrics());
        borderedText = new BorderedText(textSizePx);
        borderedText.setTypeface(Typeface.MONOSPACE);

        tracker = new MultiBoxTracker(this);

        int cropSize = TF_OD_API_INPUT_SIZE;

        try {
            options = ObjectDetector.ObjectDetectorOptions.builder()
                    .setMaxResults(10)
                    .setScoreThreshold(0.85f)
                    .setNumThreads(5)
                    .build();
            detector = ObjectDetector.createFromFileAndOptions(
                    this,
                    "demo_spectrum_model.tflite",
                    options
            );

      /*detector =
          TFLiteObjectDetectionAPIModel.create(
              this,
              TF_OD_API_MODEL_FILE,
              TF_OD_API_LABELS_FILE,
              TF_OD_API_INPUT_SIZE,
              TF_OD_API_IS_QUANTIZED);*/
            cropSize = TF_OD_API_INPUT_SIZE;

        } catch (final IOException e) {
            e.printStackTrace();
            LOGGER.e(e, "Exception initializing Detector!");
            Toast toast =
                    Toast.makeText(
                            getApplicationContext(), "Detector could not be initialized", Toast.LENGTH_SHORT);
            toast.show();
            finish();
        }

        previewWidth = size.getWidth();
        previewHeight = size.getHeight();

        sensorOrientation = rotation - getScreenOrientation();
        LOGGER.i("Camera orientation relative to screen canvas: %d", sensorOrientation);

        LOGGER.i("Initializing at size %dx%d", previewWidth, previewHeight);
        rgbFrameBitmap = Bitmap.createBitmap(previewWidth, previewHeight, Config.ARGB_8888);
        croppedBitmap = Bitmap.createBitmap(cropSize, cropSize, Config.ARGB_8888);

        frameToCropTransform =
                ImageUtils.getTransformationMatrix(
                        previewWidth, previewHeight,
                        cropSize, cropSize,
                        sensorOrientation, MAINTAIN_ASPECT);

        cropToFrameTransform = new Matrix();
        frameToCropTransform.invert(cropToFrameTransform);

        trackingOverlay = findViewById(R.id.tracking_overlay);
        tutorialView = findViewById(R.id.tutorial_view);

        trackingOverlay.addCallback(
                new DrawCallback() {
                    @Override
                    public void drawCallback(final Canvas canvas) {
                        tracker.draw(canvas);
                        if (isDebug()) {
                            tracker.drawDebug(canvas);
                        }
                    }
                });

        tracker.setFrameConfiguration(previewWidth, previewHeight, sensorOrientation);
    }

    private void resetDetected() {
        if (tracker != null) {
            isStarted = false;
            thresholdFrameCount = 0;
            tracker.trackResults(new ArrayList<Detection>(), 0);
            trackingOverlay.postInvalidate();
        }
        updateProgressStatus();
    }

    @Override
    protected void processImage() {
        ++timestamp;
        final long currTimestamp = timestamp;
        trackingOverlay.postInvalidate();

        // No mutex needed as this method is not reentrant.
        if (computingDetection) {
            readyForNextImage();
            return;
        }
        computingDetection = true;
        LOGGER.i("Preparing image " + currTimestamp + " for detection in bg thread.");

        rgbFrameBitmap.setPixels(getRgbBytes(), 0, previewWidth, 0, 0, previewWidth, previewHeight);

        readyForNextImage();

        final Canvas canvas = new Canvas(croppedBitmap);
        canvas.drawBitmap(rgbFrameBitmap, frameToCropTransform, null);

        // For examining the actual TF input.
        if (SAVE_PREVIEW_BITMAP) {
            ImageUtils.saveBitmap(croppedBitmap);
        }

        runInBackground(
                new Runnable() {
                    @Override
                    public void run() {
                        LOGGER.i("Running detection on image " + currTimestamp);
                        final long startTime = SystemClock.uptimeMillis();
                        TensorImage image = TensorImage.fromBitmap(rgbFrameBitmap);

                        final List<Detection> detectionList = detector.detect(image);

                        //final List<Detector.Recognition> results = detector.recognizeImage(croppedBitmap);
                        lastProcessingTimeMs = SystemClock.uptimeMillis() - startTime;

                        cropCopyBitmap = Bitmap.createBitmap(rgbFrameBitmap);
                        final Canvas canvas = new Canvas(cropCopyBitmap);
                        final Paint paint = new Paint();
                        paint.setColor(Color.RED);
                        paint.setStyle(Style.STROKE);
                        paint.setStrokeWidth(2.0f);

                        float minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
                        switch (MODE) {
                            case TF_OD_API:
                                minimumConfidence = MINIMUM_CONFIDENCE_TF_OD_API;
                                break;
                        }

                        // final List<Detector.Recognition> mappedRecognitions = new ArrayList<Detector.Recognition>();
                        final List<Detection> mappedRecognitions = new ArrayList<>();
                        for (final Detection detection : detectionList) {
                            final RectF location = detection.getBoundingBox();
                            if (location != null && detection.getCategories().get(0).getScore() >= minimumConfidence) {
                                canvas.drawRect(location, paint);
                                //cropToFrameTransform.mapRect(location);
                                mappedRecognitions.add(detection);
                            }

                        }
                        if (isStarted) {
                            currentFrameCount++;
                            thresholdFrameCount++;
                            if (currentFrameCount >= 10) {
                                tracker.trackResults(mappedRecognitions, currTimestamp);
                                TrackingManager.INSTANCE.addTrackedObject(thresholdFrameCount, tracker.getExpectedObjects());
                                trackingOverlay.postInvalidate();
                                currentFrameCount = 0;
                            }
                        } else {
                            //TODO Need to do it later
                            //TrackingManager.INSTANCE.lookForInstructionsIfAvailable(trackerListener);
                        }
                        computingDetection = false;
                        runOnUiThread(
                                new Runnable() {
                                    @Override
                                    public void run() {
                                        showFrameInfo(previewWidth + "x" + previewHeight);
                                        showCropInfo(cropCopyBitmap.getWidth() + "x" + cropCopyBitmap.getHeight());
                                        showInference(lastProcessingTimeMs + "ms");
                                    }
                                });
                    }
                });
    }

    @Override
    protected int getLayoutId() {
        return R.layout.tfe_od_camera_connection_fragment_tracking;
    }

    @Override
    protected Size getDesiredPreviewFrameSize() {
        return DESIRED_PREVIEW_SIZE;
    }

    // Which detection model to use: by default uses Tensorflow Object Detection API frozen
    // checkpoints.
    private enum DetectorMode {
        TF_OD_API;
    }

    @Override
    protected void setUseNNAPI(final boolean isChecked) {
        runInBackground(
                () -> {
                    try {
                        // detector.setUseNNAPI(isChecked);
                    } catch (UnsupportedOperationException e) {
                        LOGGER.e(e, "Failed to set \"Use NNAPI\".");
                        runOnUiThread(
                                () -> {
                                    Toast.makeText(this, e.getMessage(), Toast.LENGTH_LONG).show();
                                });
                    }
                });
    }

    @Override
    protected void setNumThreads(final int numThreads) {
        //runInBackground(() -> detector.setNumThreads(numThreads));
    }
}
