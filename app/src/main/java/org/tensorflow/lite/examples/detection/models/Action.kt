package org.tensorflow.lite.examples.detection.models

data class Action(
    val overlayType: String?,
    val success: Result?,
    val failure: Result?
)

data class Result(
    val title: String?,
    val description: String?,
    val componentImage: String?
)