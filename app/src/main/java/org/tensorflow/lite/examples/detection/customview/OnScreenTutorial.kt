package org.tensorflow.lite.examples.detection.customview

import android.graphics.*


class OnScreenTutorial {

    private val rectPaint = Paint()
    private val pathPaint = Paint()
    private val path = Path()

    init {
        initRectPaint()
        initTextPaint()
    }

    private fun initRectPaint() {
        rectPaint.color = Color.DKGRAY
        rectPaint.alpha = 180
        rectPaint.isAntiAlias = true
    }

    private fun initTextPaint() {
        val dashPath = DashPathEffect(floatArrayOf(10f, 1f), 2.0.toFloat())
        pathPaint.color = Color.YELLOW
        pathPaint.style = Paint.Style.STROKE
        pathPaint.pathEffect = dashPath
        pathPaint.strokeWidth = 10.0f
        pathPaint.isAntiAlias = true
    }


    fun draw(trackedObjectRect: RectF, canvas: Canvas) {
        val tutorialRect = RectF(10.0f, 10.0f, (canvas.width - 10).toFloat(), 150.0f)
        canvas.drawRect(tutorialRect, rectPaint)
        /*canvas.drawBitmap(bitmap,
            (tutorialRect.right - 80).toFloat(),
            (tutorialRect.bottom - 100).toFloat(),
            null
        )*/
        //canvas.drawBitmap(bitmap,rectBitmap,null)

        path.moveTo((tutorialRect.right - 80), (tutorialRect.bottom - 100))
        path.lineTo((trackedObjectRect.centerX()), trackedObjectRect.top)
        // path.lineTo(200f, 500f);
        //path.lineTo(200f, 300f);
        // path.lineTo(350f, 300f);
        canvas.drawPath(path, pathPaint)
        path.reset()

    }

}