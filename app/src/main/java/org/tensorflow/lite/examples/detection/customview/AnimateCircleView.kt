package org.tensorflow.lite.examples.detection.customview

import android.animation.PropertyValuesHolder
import android.animation.ValueAnimator
import android.content.Context
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.RectF
import android.os.Build
import android.util.AttributeSet
import android.view.View
import androidx.annotation.RequiresApi
import org.tensorflow.lite.examples.detection.R

class AnimateCircleView(
    context: Context?,
    attrs: AttributeSet?
) : View(context, attrs) {

    companion object {
        const val ARC_FULL_ROTATION_DEGREE = 360
        const val PERCENTAGE_DIVIDER = 100.0
        const val PERCENTAGE_VALUE_HOLDER = "percentage"
    }

    private var currentPercentage = 0


    private val ovalSpace = RectF()

    private var rectWidth = 0
    private var rectHeight = 0

    private var ovalSize = 50

    @RequiresApi(Build.VERSION_CODES.M)
    private val parentArcColor =
        context?.resources?.getColor(R.color.circle_transparent, null) ?: Color.GRAY

    @RequiresApi(Build.VERSION_CODES.M)
    private val fillArcColor =
        context?.resources?.getColor(R.color.circle_fill, null) ?: Color.GREEN

    @RequiresApi(Build.VERSION_CODES.M)
    private val parentArcPaint = Paint().apply {
        style = Paint.Style.STROKE
        isAntiAlias = true
        color = parentArcColor
        strokeWidth = 5f
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private val fillArcPaint = Paint().apply {
        style = Paint.Style.STROKE
        isAntiAlias = true
        color = fillArcColor
        strokeWidth = 8f
        strokeCap = Paint.Cap.ROUND
    }

    fun setSpace(rect: RectF, ovalSize: Int) {
        this.ovalSize = ovalSize
        rectWidth = rect.width().toInt()
        rectHeight = rect.height().toInt()
    }


    @RequiresApi(Build.VERSION_CODES.M)
    override fun onDraw(canvas: Canvas?) {
        setSpace()
        canvas?.let {
            drawBackgroundArc(it)
            drawInnerArc(it)
        }
    }

    private fun setSpace() {
        val horizontalCenter = ((rectWidth + ovalSize).div(2)).toFloat()
        val verticalCenter = ((rectHeight + ovalSize).div(2)).toFloat()
        ovalSpace.set(
            horizontalCenter,
            verticalCenter,
            horizontalCenter + ovalSize,
            verticalCenter + ovalSize
        )
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun drawBackgroundArc(it: Canvas) {
        it.drawArc(ovalSpace, 0f, 360f, false, parentArcPaint)
    }

    @RequiresApi(Build.VERSION_CODES.M)
    private fun drawInnerArc(canvas: Canvas) {
        val percentageToFill = getCurrentPercentageToFill()
        canvas.drawArc(ovalSpace, 270f, percentageToFill, false, fillArcPaint)
    }

    private fun getCurrentPercentageToFill() =
        (ARC_FULL_ROTATION_DEGREE * (currentPercentage / PERCENTAGE_DIVIDER)).toFloat()

    fun animateProgress() {
        val valuesHolder = PropertyValuesHolder.ofFloat("percentage", 0f, 100f)
        val animator = ValueAnimator().apply {
            setValues(valuesHolder)
            duration = 1500
            addUpdateListener {
                val percentage = it.getAnimatedValue(PERCENTAGE_VALUE_HOLDER) as Float
                currentPercentage = percentage.toInt()
                invalidate()
            }
        }
        animator.start()
    }
}