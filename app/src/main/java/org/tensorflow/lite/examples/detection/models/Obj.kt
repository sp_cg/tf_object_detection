package org.tensorflow.lite.examples.detection.models

import org.tensorflow.lite.examples.detection.DetectorActivity


object ProductManager {

    @JvmStatic
    val productInfo = getProductInfo()


    @JvmStatic
    fun resetProductInfo() {
        DetectorActivity.productInfo = getProductInfo()
    }
}