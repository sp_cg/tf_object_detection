package org.tensorflow.lite.examples.detection.utility

import android.content.Context
import android.speech.tts.TextToSpeech
import android.speech.tts.Voice
import android.widget.Toast
import java.util.*

class SpeechManager(val context: Context) {

    lateinit var speech: TextToSpeech

    init {
        init()
    }

    private fun init() {
        speech = TextToSpeech(
            context.applicationContext
        ) {
            if (it != TextToSpeech.ERROR) {
                speech.language = Locale.US
            }
        }
    }

    fun speak(text: String) {
        if (speech.isSpeaking) {
            return
        }
        speech.speak(
            text,
            TextToSpeech.QUEUE_FLUSH,
            null
        )
    }

    fun onDestroy() {
        if (speech != null) {
            speech.stop()
            speech.shutdown()
        }
    }
}