package org.tensorflow.lite.examples.detection.tracking

import android.util.Log
import org.tensorflow.lite.examples.detection.DetectorActivity
import org.tensorflow.lite.examples.detection.customview.Overlay
import org.tensorflow.lite.examples.detection.models.Action
import org.tensorflow.lite.examples.detection.models.Component
import org.tensorflow.lite.examples.detection.models.Step

object TrackingManager {

    private val completedSteps = mutableListOf<Step>()
    private val trackedObjects = mutableListOf<MultiBoxTracker.TrackedRecognition>()
    var currentStep: Step? = null
    lateinit var callback: TrackerListener

    fun setUp(callback: TrackerListener) {
        this.callback = callback
    }


    fun lookForInstructionsIfAvailable(callback: TrackerListener) {
        if (currentStep == null) currentStep = getCurrentStepsObj()
        this.callback.initiateInstruction(getInstructionsForCurrentStep())
    }


    fun addTrackedObject(
        currentThresholdFrameCount: Int,
        trackedObjects: List<MultiBoxTracker.TrackedRecognition>
    ) {
        this.trackedObjects.clear()
        this.currentStep = getCurrentStepsObj()
        if (trackedObjects.isEmpty()) {
            callback.hideOverlayIfShowing()
            if (currentThresholdFrameCount >= DetectorActivity.MAX_FRAME_COUNT) {
                callback.showNoItemsDetectedError(currentStep!!)
            }
            return
        }
        this.trackedObjects.addAll(trackedObjects)
        checkForAction(currentThresholdFrameCount)
    }

    private fun checkForAction(currentThresholdFrameCount: Int) {
        val isValidationRequired = isValidationRequiredForCurrentStep()
        if (isValidationRequired) {
            val isObjectAvailableInSuccess = isObjectAvailableInSuccessList()
            if (isObjectAvailableInSuccess) {
                callback.showObjectDetectionResult(true, currentStep!!, trackedObjects[0])
                return
            }
            val isObjectAvailableInFailure = isObjectAvailableInFailureList()
            if (isObjectAvailableInFailure) {
                callback.showObjectDetectionResult(false, currentStep!!, trackedObjects[0])
                return
            }
        } else {
            if (isAllExpectedObjectsDetected() || currentThresholdFrameCount >= DetectorActivity.MAX_FRAME_COUNT) {
                currentStep?.let { currentStep ->
                    if (currentStep.stepName == Step.SCAN_PRODUCT_COMPONENTS) {
                        callback.showProductComponentScanResult(
                            getProductRequiredComponentScanResult(),
                            TrackingManager.currentStep!!
                        )
                    } else {
                        callback.showObjectDetectionResult(
                            true,
                            currentStep,
                            trackedObjects[0]
                        )
                    }
                    return
                }
                callback.showGeneralError(currentStep!!)
            }
        }
    }

    private fun getProductRequiredComponentScanResult(): List<Component> {
        val list = mutableListOf<Component>()
        val expectedList = currentStep?.requiredItems
        expectedList?.let { requiredList ->
            for (requiredTitle in requiredList) {
                val isDetected = (trackedObjects.any { it.title == requiredTitle })
                list.add(
                    Component(
                        requiredTitle,
                        isDetected
                    )
                )
            }
        }
        return list
    }


    fun isLabelInSuccessList(label: String) = currentStep?.positiveItems?.contains(label) ?: false
    fun isLabelInFailureList(label: String) = currentStep?.negativeItems?.contains(label) ?: false

    private fun isObjectAvailableInSuccessList(): Boolean {
        for (trackedObject in trackedObjects) {
            if (isLabelInSuccessList(trackedObject.title)) return true
        }
        return false
    }

    private fun isObjectAvailableInFailureList(): Boolean {
        for (trackedObject in trackedObjects) {
            if (isLabelInFailureList(trackedObject.title)) return true
        }
        return false
    }


    fun moveToNextStep() {
        DetectorActivity.productInfo =
            DetectorActivity.productInfo.copy(currentStep = getCurrentStepIndex() + 1)

        currentStep?.let { completedSteps.add(it) }
        currentStep = getCurrentStepsObj()
    }


    fun getActionForCurrentStep(): Action? {
        return currentStep?.action
    }

    fun getCurrentStepOverlayType() =
        currentStep?.action?.overlayType ?: Overlay.OVERLAY_SIMPLE_MESSAGE_AUTO_DISMISS


    fun isObjectAvailableInRequiredItems(label: String): Boolean {
        val step = getCurrentStepsObj()
        step?.let {
            return it.requiredItems.contains(label)
        }
        return false
    }

    fun isLastStep(): Boolean = isAllStepsCompleted()

    private fun isAllStepsCompleted(): Boolean {
        return DetectorActivity.productInfo.currentStep == DetectorActivity.productInfo.steps.size
    }


    fun getInstructionsForCurrentStep(): List<String>? {
        val currentStep = getCurrentStepsObj()
        currentStep?.let {
            return it.instructions
        }
        return null
    }

    fun getActionInStringFormat(): String {
        val action = getActionForCurrentStep()
        var stringBuilder = StringBuilder()
        action?.success?.let {
            stringBuilder.append(it.title)
        }

        return stringBuilder.toString()
    }

    fun getCurrentStepsObj(): Step? {
        val index = if (getCurrentStepIndex() == 0) 1 else getCurrentStepIndex()
        return DetectorActivity.productInfo.steps.find { it.stepId == index }
    }

    fun getCurrentStepIndex() = DetectorActivity.productInfo.currentStep


    fun getComponentsInString(): String {
        val currentStep = getCurrentStepsObj()
        val listdata = currentStep?.requiredItems
        var stringBuilder = StringBuilder()
        if (!listdata.isNullOrEmpty()) {
            for (data: String in listdata) {
                stringBuilder.append(data).append("\n")
            }
        }
        return stringBuilder.toString()
    }

    fun getInstructionsInString(): String {
        Log.i(javaClass.simpleName, "Current Step: ${getCurrentStepIndex()}")
        val currentStep =
            DetectorActivity.productInfo.steps.find { it.stepId == getCurrentStepIndex() }
        val instructions = currentStep?.instructions
        var stringBuilder = StringBuilder()
        if (!instructions.isNullOrEmpty()) {
            for (instruction: String in instructions) {
                stringBuilder.append(instruction).append("\n")
            }
        }
        return stringBuilder.toString()

    }


    fun isValidationRequiredForCurrentStep(): Boolean {
        currentStep?.let {
            return it.isValidationRequired
        }
        return false
    }


    private fun isAllExpectedObjectsDetected(): Boolean {
        Log.i(javaClass.simpleName, "Tracked Object Size: ${trackedObjects.size}")

        if (trackedObjects.isEmpty()) {
            return false
        }
        val items = mutableListOf<String>()
        for (trackedObject in trackedObjects) {
            Log.i(javaClass.simpleName, "Tracked Object Title: ${trackedObject.title}")
            items.add(trackedObject.title)
        }
        currentStep?.let {
            val isContains = items.containsAll(it.requiredItems)
            Log.i(javaClass.simpleName, "Is All contains: $isContains")
            return isContains
        }
        return false
    }
}

interface TrackerListener {
    fun initiateInstruction(instructions: List<String>?)
    fun showProductComponentScanResult(
        results: List<Component>,
        currentStep: Step
    )

    fun showObjectDetectionResult(
        isObjectMatched: Boolean,
        currentStep: Step,
        detectedObject: MultiBoxTracker.TrackedRecognition
    )

    fun hideOverlayIfShowing()
    fun showNoItemsDetectedError(currentStep: Step)
    fun showGeneralError(currentStep: Step)
}