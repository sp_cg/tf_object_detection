package org.tensorflow.lite.examples.detection.customview

import android.content.Context
import android.graphics.*
import android.text.TextPaint
import android.util.AttributeSet
import android.widget.FrameLayout
import org.tensorflow.lite.examples.detection.tracking.TrackingManager
import org.tensorflow.lite.examples.detection.utility.Utility


class TutorialView(
    context: Context,
    attributeSet: AttributeSet
) : FrameLayout(context, attributeSet) {


    fun drawTutorial(trackedRectF: RectF, canvasWidth: Int) {
        val componentBitmap =
            Utility.getBitmapFromBase64(TrackingManager.getActionForCurrentStep()?.success?.componentImage)
        val tutorialRectF = RectF(10.0f, 10.0f, (canvasWidth - 10).toFloat(), 150.0f)

        removeAllViews()

        val animateCircleView = AnimateCircleView(context, null)
        val circleSize = 120
        animateCircleView.setSpace(trackedRectF, circleSize)

        animateCircleView.x = trackedRectF.left - circleSize
        animateCircleView.y = trackedRectF.top - circleSize

        addView(animateCircleView)
        animateCircleView.animateProgress()

        val imageView = ImageWithLabel(context, null)
        imageView.setCanvasData(canvasWidth - 10, componentBitmap!!)
        imageView.x = 0f
        imageView.y = 0f
        addView(imageView)


        val lineView = AnimatedLineView(context, null)
        lineView.setCoordinates(trackedRectF, tutorialRectF, componentBitmap)
        addView(lineView)

    }

    fun resetTutorial() {
        removeAllViews()
    }
}