package org.tensorflow.lite.examples.detection.customview

import android.app.Activity
import android.view.View
import android.widget.TextView
import org.tensorflow.lite.examples.detection.R

class SimpleOverlay(
    private val activity: Activity,
    private val view: View
) : Overlay {

    private val textView = view.findViewById<TextView>(R.id.txt_message)

    override fun showOverlay(message: String) {
        activity.runOnUiThread {
            view.visibility = View.VISIBLE
            textView.text = message
        }

    }

    // Make sure this function is called from UI thread
    override fun hide() {
        activity.runOnUiThread {
            if (view.visibility == View.VISIBLE) {
                view.visibility = View.GONE
            }
        }
    }
}