package org.tensorflow.lite.examples.detection.dialogs

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.DialogFragment
import org.tensorflow.lite.examples.detection.databinding.DialogComponentsBinding
import org.tensorflow.lite.examples.detection.tracking.TrackingManager

class ComponentsDialog : DialogFragment() {

    var binding: DialogComponentsBinding? = null;


    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        return super.onCreateDialog(savedInstanceState)
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DialogComponentsBinding.inflate(layoutInflater)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        binding?.txtComponents?.text = TrackingManager.getComponentsInString()

        binding?.buttonNext?.setOnClickListener {
            if((it as Button).text == "OK"){
                dialog?.dismiss()
            }else{
                updateOnNext()
            }

        }
    }

    private fun updateOnNext() {
        TrackingManager.moveToNextStep()

        binding?.txtTitle?.text = "Next Step is (${TrackingManager.getCurrentStepIndex()})"
        binding?.txtComponents?.text = TrackingManager.getInstructionsInString()

        binding?.buttonNext?.text = "OK"
    }
}